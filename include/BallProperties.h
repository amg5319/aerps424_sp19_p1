/*
 * BallProperties.hpp
 *
 *  Created on: Mar 30, 2019
 *      Author: andrew
 */

#ifndef INCLUDE_BALLPROPERTIES_HPP_
#define INCLUDE_BALLPROPERTIES_HPP_

/**
 * Simple structure to contain properties of the pool balls
 */
struct BallProperties{
	static constexpr const float radius = 0.025f;
};
#endif /* INCLUDE_BALLPROPERTIES_HPP_ */
