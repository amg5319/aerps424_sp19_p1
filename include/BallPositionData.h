/*
 * BallPositionData.hpp
 *
 *  Created on: Mar 30, 2019
 *      Author: andrew
 */

#ifndef INCLUDE_BALLPOSITIONDATA_HPP_
#define INCLUDE_BALLPOSITIONDATA_HPP_

#include <vector>
#include <random>
#include <chrono>


/**
 * Data Structure containing a vector of all pool Balls in the simulation.
 */
class BallPositionData {
public:

	/**
	 * Internal Data Structure formalizing the concept of a pool ball
	 * having a position, velocity, and color.
	 */
	struct Ball {

		/**
		 * Ball constructor initializes position give inputs and sets
		 * initial velocities to zero and color to be white.
		 * @param inX (I) input x coordinate
		 * @param inY (I) input y coordinate
		 */
		Ball(const float inX = 0.0f, const float inY = 0.0f);

		float xPos, yPos;  /**< Position x and y coordinates */
		float xVel, yVel;  /**< Velocity for x and y */
		float R, G, B;     /**< RGB color values */
	};

	/**
	 * Construct the simulation data structure with given number of balls
	 * @param n (I) number of balls that are not the cue ball
	 */
	BallPositionData(const unsigned int n);

	/**
	 * Print the x and y coordinate position of the randomly placed balls.
	 * The position of the cue ball is listed first.
	 */
	void printBallPositions() const;

	/**
	 * Provide a vector of pointers to all Balls that are moving
	 */
	std::vector<Ball*> getMovingBalls();

	/**
	 * Provide a vector of pointers to all Balls contained within this
	 * data structure
	 */
	std::vector<Ball*> getAllBalls();

private:

	std::mt19937_64 generator;     /**< Mersenne Twister rng engine */
	std::uniform_real_distribution<float> xDistribution;  /**< distribution for x-coordinate placement  */
	std::uniform_real_distribution<float> yDistribution;  /**< distribution for y-coordinate placement  */
	std::uniform_real_distribution<float> colorDistribution; /**< distribution for random color */
	std::vector<Ball> ballData_;   /**< the main container of all simulation data */
};



#endif /* INCLUDE_BALLPOSITIONDATA_HPP_ */
