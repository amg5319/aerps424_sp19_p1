/*
 * StrikeProperties.hpp
 *
 *  Created on: Mar 30, 2019
 *      Author: andrew
 */

#ifndef INCLUDE_STRIKEPROPERTIES_HPP_
#define INCLUDE_STRIKEPROPERTIES_HPP_

/**
 * Simple structure to define the properties of a pool cue strike
 */
struct StrikeProperties{
	static constexpr const float impulse = 0.01f;
};



#endif /* INCLUDE_STRIKEPROPERTIES_HPP_ */
