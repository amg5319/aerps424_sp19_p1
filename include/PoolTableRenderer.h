/*
 * PoolTableRenderer.hpp
 *
 *  Created on: Mar 30, 2019
 *      Author: andrew
 */

#ifndef INCLUDE_POOLTABLERENDERER_HPP_
#define INCLUDE_POOLTABLERENDERER_HPP_
//#include <GL/glut.h>
//#include <GL/gl.h>
#include <GL/glut.h>
#include <GL/GL.h>

#include "../include/BallPositionData.h"

/**
 * The class is responsible for drawing the pool table in a X-Window using
 * openGL and for handling the pool ball kinematics.
 */
class PoolTableRenderer {

	//pointer to our data object with all
	//information on our pool balls
	static BallPositionData* data_;

	/**
	 * Create the window and inform GLUT of our displayfunction (drawScene)
	 * Handlers for window resizing and keyboard inputs are also set.
	 */
	static void initWindowProperties();

	/**
	 * Draw our pool table surface
	 * @param z    (I) depth of the table surface
	 */
	static void drawTable(const float z);

	/**
	 * Draw a single ball given its position and color (RGB)
	 * @param x   (I) x coordinate
	 * @param y   (I) y coordinate
	 * @param z   (I) z coordinate : const -1.0f;
	 * @param r   (I) red color value
	 * @param g   (I) green color value
	 * @param b   (I) bluee color value
	 */
	static void drawBall(const float x, const float y, const float z,
			             const float r, const float g, const float b);

	/**
	 * Draws one frame of the animation.
	 */
	static void drawScene();

	/**
	 * Handler for Keyboard input
	 * @param key  (I)  keyboard button pressed
	 * @param x (I) unused
	 * @param y (I) unused
	 */
	static void keyPress(int key, int x, int y);

	/**
	 * Handler for resizing the window.
	 * @param w (I) window width in pixels
	 * @param h (I) window height in pixels
	 */
	static void handleResize(int w, int h);

	/**
	 * The main update, which updates the position and velocity of each
	 * ball depending on the kinematics. Collisions are handled elastically
	 * and assume to preserve linear momentum.  The speed of the balls is
	 * reduced every 25ms assuming 1% damping per component.  If a ball's
	 * speed reduces below 1e-4 non-dimensional speed, it is halted.
	 *
	 * @param value  (I) unused
	 */
	static void update(int value);

public:

	/**
	 * Initializes GLUT to make the Table Rendering window.
	 * @param argc    (I) number of input arguments from the command line
	 * @param argv    (I) the command line arguments
	 * @param input   (I) pointer to our data structure
	 *
	 */
	PoolTableRenderer(int* argc, char** argv, BallPositionData*);

	/**
	 * The most important function as it begins the whole simulation.
	 * It begins by initializing the window properties
	 * and then entering the main GLUT loop.
	 */
	void beginSim();
};

#endif /* INCLUDE_POOLTABLERENDERER_HPP_ */
