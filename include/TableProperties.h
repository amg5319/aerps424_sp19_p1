/*
 * TableProperties.hpp
 *
 *  Created on: Mar 30, 2019
 *      Author: andrew
 */

#ifndef INCLUDE_TABLEPROPERTIES_HPP_
#define INCLUDE_TABLEPROPERTIES_HPP_

/**
 * Simple structure to contain properties of the pool table
 */
struct TableProperties{

	static constexpr const float length = 1.6f;
	static constexpr const float width = 0.8f;

};


#endif /* INCLUDE_TABLEPROPERTIES_HPP_ */
