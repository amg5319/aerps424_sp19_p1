/*
 * source.cpp
 *
 *  Created on: Mar 27, 2019
 *      Author: andrew
 *
 * This code was written for fun to demonstrate a possible solution for the
 * first project in AERSP 424 in Spring 2019.  It was written by the graduate
 * teaching assistant using C++14 and openGL.
 */
#include <iostream>
#include "../include/BallPositionData.h"
#include "../include/PoolTableRenderer.h"
using namespace std;

int main(int argc, char** argv){

	cout << "================================================\n"
	     << " WELCOME TO POOL SIMULATER 3000                 \n"
		 << "================================================\n"
		 << "Use the arrow keys to strike the Cue Ball with a medium strength shot.\n"
		 << "Hold CTRL with an arrow key for a weak shot.\n"
		 << "Hold SHIFT with an arrow key to do a power shot.\n"
		 << "Please enter a number of balls to simulate: ";

	// take user inpur of number of balls
	unsigned int num;
	cin >> num;

	//generate data
	BallPositionData myData(num+1);

	//output randomly generated ball positions to the console
	myData.printBallPositions();

	//create our simulation engine
	PoolTableRenderer sim(&argc, argv, &myData);

	//begin the sim.
	sim.beginSim();

    return 0;
}

