/*
 * BallPositionData.cpp
 *
 *  Created on: Mar 30, 2019
 *      Author: andrew
 */

#include "../include/BallPositionData.h"
#include "../include/BallProperties.h"
#include "../include/TableProperties.h"

#include <iostream>
#include <cmath>

/**
 * Ball constructor initializes position give inputs and sets
 * initial velocities to zero and color to be white.
 * @param inX (I) input x coordinate
 * @param inY (I) input y coordinate
 */
BallPositionData::Ball::Ball(const float inX, const float inY):
	xPos{inX}, yPos{inY}, xVel{0.0f}, yVel{0.0f}, R{1.0f}, G{1.0f}, B{1.0f}
{
}

/**
 * Construct the simulation data structure with given number of balls
 * @param n (I) number of balls that are not the cue ball
 */
BallPositionData::BallPositionData(const unsigned int n) :
	generator(std::chrono::system_clock::now().time_since_epoch().count()),
	xDistribution(-TableProperties::length/2 + 2*BallProperties::radius, TableProperties::length/2 - 2*BallProperties::radius),
	yDistribution(-TableProperties::width/2 + 2*BallProperties::radius, TableProperties::width/2 - 2*BallProperties::radius),
	colorDistribution(0.0f, 1.0f),
	ballData_(n)
{
	bool isFirstBall = true;  // The first ball is the cue ball, so we want to leave it white
	for(auto& ball : ballData_){

		//generate the position of this ball
		ball.xPos = xDistribution(generator);
		ball.yPos = yDistribution(generator);

		if(isFirstBall){
		    //first ball is left white
			isFirstBall = false;
		}else{

			//all other balls are given a color.  The green RGB parameter is left
			// 0.0f so that all balls will be some shade of red or blue to stick out
			// from the green board.
			ball.R = colorDistribution(generator);
			ball.G = 0.0f; // I want the balls to stand out from the green table.
			ball.B = colorDistribution(generator);
		}

	}
	//vectors allocate more memory than they need.  Since we will not be adding
	//new balls, it's best to free this memory back to the system.
	ballData_.shrink_to_fit();
}

/**
 * Print the x and y coordinate position of the randomly placed balls.
 * The position of the cue ball is listed first.
 */
void BallPositionData::printBallPositions() const
{
	//simply iterate through each ball and print its position
	for(auto ball : ballData_){
		std::cout << ball.xPos << "   " << ball.yPos << std::endl;
	}

}

/**
 * Provide a vector of pointers to all Balls that are moving
 */
std::vector<BallPositionData::Ball*> BallPositionData::getMovingBalls()
{
	std::vector<Ball*> output;
	for(auto& ball : ballData_){
		//If either the velocity in x or y directions is non-zero, then
		//this ball is moving and is added to the output vector.
		if(std::fabs(ball.xVel) > 0.0f || std::fabs(ball.yVel) > 0.0f){
			output.push_back(&ball);
		}
	}
	return output;
}

/**
 * Provide a vector of pointers to all Balls contained within this
 * data structure
 */
std::vector<BallPositionData::Ball*> BallPositionData::getAllBalls()
{
	std::vector<Ball*> output;
	//All balls added to the output vector.
	for(auto& ball : ballData_){
		output.push_back(&ball);
	}
	return output;
}

