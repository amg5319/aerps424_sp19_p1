/*
 * PoolTableRenderer.cpp
 *
 *  Created on: Mar 30, 2019
 *      Author: andrew
 *
 * I made this program for fun to demonstrate to the students in
 * AERSP 424 for the Spring 2019 semester the possibilities of a
 * pool ball simulation code.
 */

#include "../include/PoolTableRenderer.h"
#include "../include/BallProperties.h"
#include "../include/StrikeProperties.h"
#include "../include/TableProperties.h"


BallPositionData* PoolTableRenderer::data_ = nullptr;

/**
 * Initializes GLUT to make the Table Rendering window.
 * @param argc    (I) number of input arguments from the command line
 * @param argv    (I) the command line arguments
 * @param input   (I) pointer to our data structure
 */
PoolTableRenderer::PoolTableRenderer(int* argc, char** argv,
		                             BallPositionData* input)
{
	data_ = input;

	//Initialize the GLUT library.
	//negotiates a session with windowing system.
	//parses X-Window options from the command line
    glutInit(argc, argv);

    //set the initial display mode
    //GLUT_DOUBLE : double buffered window.  Double buffering reduces graphics
    //              flicker.
    //GLUT_RGB : RGBA color model.  no alpha is selected though.
    //GLUT_DEPTH : Window has a depth buffer.  So I can set the table
    //             behind the balls.
    glutInitDisplayMode(GLUT_DOUBLE|GLUT_RGB|GLUT_DEPTH);

    //set Window size to be 1000x1000 pixels.
    glutInitWindowSize(1000,1000);
}

/**
 * Draws one frame of the animation.
 */
void PoolTableRenderer::drawScene()
{
	//Clear the window display buffer
	//GL_COLOR_BUFFER_BIT : indicates the buffer has color enables
	//GL_DEPTH_BUFFER_BIT : indicates there is a depth buffer
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Specify which translation matrix is being used to draw.
	//GL_MODELVIEW : All matrix operations (displacements/rotations) are
	//               in the modelview coordinates
	glMatrixMode(GL_MODELVIEW);

	//clears any current displacement matrix to be an identity matrix.
	glLoadIdentity();

	//call function to draw our pool table
	drawTable(-1.0f-BallProperties::radius);

	//iterate through all ball positions and draw them on top of the pool table.
	//ball positions are closer to the camera than the table by one ball radius
	for(auto ball : data_->getAllBalls()){
		drawBall(ball->xPos,ball->yPos, -1.0f, ball->R, ball->G, ball->B);
	}

	//swap buffers of the current window if it is double buffered, which
	//our window is.
	glutSwapBuffers();
}

/**
 * Draw our pool table surface
 * @param z    (I) depth of the table surface
 */
void PoolTableRenderer::drawTable(const float z)
{

	//set the color of our table to be a pool table green
	//set up the lighting of the material
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	GLfloat tableGreen[] = {10.0f/255.0f,108.0f/255.0f,3.0f/255.0f, 1.0f};
	//glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, tableGreen);
	//glMaterialfv(GL_FRONT_AND_BACK, GL_SPECULAR, tableGreen);
	glColor3f(tableGreen[0], tableGreen[1], tableGreen[2]);

	//draw our rectangle
	glBegin(GL_QUADS);
	glNormal3f(0.0f, 0.0f, 1.0f);
	glVertex3f(-TableProperties::length/2, -TableProperties::width/2, z);
	glVertex3f(TableProperties::length/2, -TableProperties::width/2, z);
	glVertex3f(TableProperties::length/2, TableProperties::width/2, z);
	glVertex3f(-TableProperties::length/2, TableProperties::width/2, z);
	glEnd();

	//move our origin back to its original position in modelview
}

/**
 * Draw a single ball given its position and color (RGB)
 * @param x   (I) x coordinate
 * @param y   (I) y coordinate
 * @param z   (I) z coordinate : const -1.0f;
 * @param r   (I) red color value
 * @param g   (I) green color value
 * @param b   (I) bluee color value
 */
void PoolTableRenderer::drawBall(const float x, const float y, const float z,
		                         const float r, const float g, const float b)
{
	//set the color this ball
	//set the color of our table to be a pool table green
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);
	GLfloat shine[] = {128.0f};
	glMaterialfv(GL_FRONT_AND_BACK, GL_SHININESS, shine);
	glColor3f(r,g,b);

	//translate the origin to the location of the current ball
	glTranslatef(x, y, z);

	//Draw the sphere at this location.  The sphere uses 100 sides per axis for a
	//smooth surface.
	glutSolidSphere(BallProperties::radius, 100, 100);

	//translate back to the modelview origin (center of our window).
	glTranslatef(-x, -y, -z);
}

/**
 * Create the window and inform GLUT of our display function (drawScene).
 * Handlers for window resizing and keyboard inputs are also set.
 */
void PoolTableRenderer::initWindowProperties()
{
	//Create the window and give it a name
	glutCreateWindow("Pool Table");

	//enable the Depth buffer to do depth comparisons.
	//how we can have the table behind our balls
	glEnable(GL_DEPTH_TEST);

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_COLOR_MATERIAL);
	GLfloat global_ambient[] = { 0.4f, 0.4f, 0.4f, 1.0f };
	glShadeModel(GL_SMOOTH);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, global_ambient);

	//Add a positioned light
	GLfloat lightColor0[] = {1.0f, 1.0f, 1.0f, 1.0f};
	GLfloat lightPosition0[] = {0.0f, 0.0f, -1.0f, 0.0f};
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor0);
	//glLightfv(GL_LIGHT0, GL_SPECULAR, lightColor0);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition0);

	//give the function that will draw one frame of animation
    glutDisplayFunc(drawScene);

    //Create keyboard callbacks for button presses.
    glutSpecialFunc(keyPress);

    //Give function to define how window resizes.
    glutReshapeFunc(handleResize);

    //set the timer for our window to update every 25 miliseconds
    //update is the function to be called taking one int parameter,
    //which is 0 for this case since it is unused.
    glutTimerFunc(25, update, 0);
}


/**
 * Handler for resizing the window.
 * @param w (I) window width in pixels
 * @param h (I) window height in pixels
 */
void PoolTableRenderer::handleResize(int w, int h)
{
	//sets the transformation between window coordinates and normalized device
	//coordinates.
	glViewport(0,0, w,h);

	//apply any matrix operations to the projection stack.
	glMatrixMode(GL_PROJECTION);

	//set the transform matrix to identity
	glLoadIdentity();

	//set up the perspective projection matrix
	// first parameter is the view angle in degrees in the y direction.
	// second is the aspect ratio
	// third is the distance from the viewer to the closest clipping plane.
	// fourth is the distance from the viewer to the far clipping plane.
	gluPerspective(90.0,
			       static_cast<double>(w) / static_cast<double>(h),
				   1.0,
				   10.0);
}

/**
 * The main update, which updates the position and velocity of each
 * ball depending on the kinematics. Collisions are handled elastically
 * and assume to preserve linear momentum.  The speed of the balls is
 * reduced every 25ms assuming 1% damping per component.  If a ball's
 * speed reduces below 1e-4 nondimensional speed, it is halted.
 *
 * @param value  (I) unused
 */
void PoolTableRenderer::update(int value)
{
	const float frictionCoeff = 0.98f;
	const float wallDamping = 0.70f;
	// iterate through all moving balls, update their positions and velocities,
	// and finally check if they collide with any of the pool table boundaries.
	// collisions with the boundary are not completely elastic and absorb 30%
	// of the ball's momentum
	for(auto ball : data_->getMovingBalls() ){
		//update x-components
		ball->xPos += ball->xVel;
		ball->xVel *= frictionCoeff;

		//update y-components
		ball->yPos += ball->yVel;
		ball->yVel *= frictionCoeff;

		//check ball speed to determine if it stops
		if(std::sqrt(ball->xVel*ball->xVel + ball->yVel*ball->yVel) < 1.0e-4){
			ball->xVel = 0.0f;
			ball->yVel = 0.0f;
		}

		//Boundary checks
	    //Check the right boundary
		if(ball->xPos > (TableProperties::length/2-2*BallProperties::radius)){
			ball->xPos = (TableProperties::length/2-2*BallProperties::radius);
			ball->xVel *= -wallDamping;
		}
		//Check the left boundary
		if(ball->xPos < (-TableProperties::length/2+2*BallProperties::radius)){
			ball->xPos = (-TableProperties::length/2+2*BallProperties::radius);
			ball->xVel *= -wallDamping;
		}

		//Check the top boundary
		if(ball->yPos > (TableProperties::width/2-2*BallProperties::radius)){
			ball->yPos = (TableProperties::width/2-2*BallProperties::radius);
			ball->yVel *= -wallDamping;
		}
		//Check the bottom boundary
		if(ball->yPos < (-TableProperties::width/2+2*BallProperties::radius)){
			ball->yPos = (-TableProperties::width/2+2*BallProperties::radius);
			ball->yVel *= -wallDamping;
		}

	}

	//obtain a vector of pointers to all pool balls
	auto allBalls = data_->getAllBalls();

	//iterate through all of the balls, and see if they come into contact with
	//any of the other balls
	for( auto focus = allBalls.begin(); focus != allBalls.end(); ++focus){
		for( auto ball = focus+1; ball != allBalls.end(); ++ball ){

			//calculate the distance vector and its magnitude of the focus ball
			//to the other ball
			float xVec = (*ball)->xPos-(*focus)->xPos;
			float yVec = (*ball)->yPos-(*focus)->yPos;
			float dist = std::sqrt( xVec*xVec+yVec*yVec );

			if(dist <= 2.01*BallProperties::radius){

				//make sure the balls don't clip into each other need to project the focus back along
				//the difference vector
				xVec /= dist;
				yVec /= dist;
				(*focus)->xPos = (*ball)->xPos - 2.01f*BallProperties::radius*xVec;
				(*focus)->yPos = (*ball)->yPos - 2.01f*BallProperties::radius*yVec;


				//get the speed of each ball
				float vfocus = std::sqrt( (*focus)->xVel*(*focus)->xVel+
					                      (*focus)->yVel*(*focus)->yVel);
				float vball = std::sqrt( (*ball)->xVel*(*ball)->xVel+
					                      (*ball)->yVel*(*ball)->yVel);

				//balls' with the most momentum are the ones keeping the most after the transfer.
				//Simply just apply a delta-V in the anti-direction
				//of the distance vector between two balls with magnitude equal to the parallel component
				//of the ball with the highest momentum's velocity in the direction of the distance vector.
				if(vfocus > vball){
					float angle = std::acos( (xVec*(*focus)->xVel+yVec*(*focus)->yVel) / vfocus);
					//important to make sure the arccos is defined.  Things could blow up otherwise.
					if(!std::isnan(angle)){
						(*ball)->xVel = (*ball)->xVel+vfocus*std::cos(angle)*xVec;
						(*ball)->yVel = (*ball)->yVel+vfocus*std::cos(angle)*yVec;

						(*focus)->xVel = (*focus)->xVel-vfocus*std::cos(angle)*xVec;
						(*focus)->yVel = (*focus)->yVel-vfocus*std::cos(angle)*yVec;
					}
				}else{
					float angle = std::acos( (xVec*(*ball)->xVel+yVec*(*ball)->yVel) / vball);
					if(!std::isnan(angle)){
						(*focus)->xVel = (*focus)->xVel+vball*std::cos(angle)*xVec;
						(*focus)->yVel = (*focus)->yVel+vball*std::cos(angle)*yVec;

						(*ball)->xVel = (*ball)->xVel-vball*std::cos(angle)*xVec;
						(*ball)->yVel = (*ball)->yVel-vball*std::cos(angle)*yVec;
					}
				}
			}
		}
	}

	//mark the current window to need an update
	glutPostRedisplay();

	//set the next update to occur in 25 ms and use this same function.
	glutTimerFunc(25,update,0);

}

/**
 * Handler for Keyboard input
 * @param key  (I)  keyboard button pressed
 * @param x (I) unused
 * @param y (I) unused
 */
void PoolTableRenderer::keyPress(int key, int x, int y)
{

	//obtain a pointer to ball number 0 (our cue ball)
	auto primaryBall = data_->getAllBalls()[0];

	const int mediumMod = 3;
	const int hardMod = 6;
	//Modifiers for our directions are
	//GLUT_ACTIVE_CTRL : the control key makes a weak strike
	//no modifier : makes a medium strike
	//GLUT_ACTIVE_SHIFT : shift makes a powerful strike

	//Check for impulses to the cue ball in the horizontal directions
	if(key == GLUT_KEY_RIGHT && glutGetModifiers() == GLUT_ACTIVE_CTRL){
		primaryBall->xVel = StrikeProperties::impulse;
	}

	if(key == GLUT_KEY_RIGHT){
		primaryBall->xVel = mediumMod *StrikeProperties::impulse;
	}

	if(key == GLUT_KEY_RIGHT && glutGetModifiers() == GLUT_ACTIVE_SHIFT){
		primaryBall->xVel = hardMod *StrikeProperties::impulse;
	}

	if(key == GLUT_KEY_LEFT && glutGetModifiers() == GLUT_ACTIVE_CTRL){
		primaryBall->xVel = -StrikeProperties::impulse;
	}

	if(key == GLUT_KEY_LEFT){
		primaryBall->xVel = -mediumMod *StrikeProperties::impulse;
	}

	if(key == GLUT_KEY_LEFT && glutGetModifiers() == GLUT_ACTIVE_SHIFT){
		primaryBall->xVel = -hardMod *StrikeProperties::impulse;
	}

	//Check for impulses to the cue ball in the vertical directions
	if(key == GLUT_KEY_UP && glutGetModifiers() == GLUT_ACTIVE_CTRL){
		primaryBall->yVel = StrikeProperties::impulse;
	}

	if(key == GLUT_KEY_UP){
		primaryBall->yVel = mediumMod *StrikeProperties::impulse;
	}

	if(key == GLUT_KEY_UP && glutGetModifiers() == GLUT_ACTIVE_SHIFT){
		primaryBall->yVel = hardMod *StrikeProperties::impulse;
	}

	if(key == GLUT_KEY_DOWN && glutGetModifiers() == GLUT_ACTIVE_CTRL){
		primaryBall->yVel = -StrikeProperties::impulse;
	}

	if(key == GLUT_KEY_DOWN){
		primaryBall->yVel = -mediumMod *StrikeProperties::impulse;
	}

	if(key == GLUT_KEY_DOWN && glutGetModifiers() == GLUT_ACTIVE_SHIFT){
		primaryBall->yVel = -hardMod *StrikeProperties::impulse;
	}


	//mark this window buffer for redisplay
	glutPostRedisplay();


}


/**
 * The most important function as it begins the whole simulation.
 * It begins by initializing the window properties
 * and then entering the main GLUT loop.
 */
void  PoolTableRenderer::beginSim()
{
	//initialize Window properties
	initWindowProperties();

	//begin the main animation loop.  This loop is terminated by
	//closing the window.
	glutMainLoop();
}
